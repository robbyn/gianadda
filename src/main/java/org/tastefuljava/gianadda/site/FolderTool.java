package org.tastefuljava.gianadda.site;

import java.util.List;
import org.tastefuljava.gianadda.domain.Folder;
import org.tastefuljava.gianadda.util.Configuration;
import org.tastefuljava.jedo.Session;

public class FolderTool {
    private final Session sess;
    private final Configuration conf;

    FolderTool(Session sess, Configuration conf) {
        this.sess = sess;
        this.conf = conf;
    }

    public Folder getRoot() {
        return sess.queryOne(Folder.class, "root", "/");
    }

    public List<Folder> latest(int count) {
        return sess.query(Folder.class, "latest", count);
    }
}

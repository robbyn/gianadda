package org.tastefuljava.gianadda.domain;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.tastefuljava.gianadda.geo.LatLng;
import org.tastefuljava.gianadda.geo.LatLngBounds;
import org.tastefuljava.gianadda.util.Util;

public class Folder {
    public static Comparator<Folder> NAME_ORDER
            = (a,b) -> Util.compareString(a.name, b.name);

    private int id;
    private Folder parent;
    private String name;
    private Date dateTime;
    private Date pubDate;
    private String title;
    private String analytics;
    private String disqus;
    private String link;
    private String description;
    private String body;
    private final SortedSet<Folder> folders = new TreeSet<>(NAME_ORDER);
    private final SortedSet<Picture> pictures = new TreeSet<>(Picture.NAME_ORDER);
    private final SortedSet<Track> tracks = new TreeSet<>(Track.NAME_ORDER);
    private final SortedSet<String> tags
            = new TreeSet<>((a,b)->Util.compareString(a, b));

    public Folder() {
    }

    public int getId() {
        return id;
    }

    public Folder getParent() {
        return parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAnalytics() {
        return analytics;
    }

    public void setAnalytics(String analytics) {
        this.analytics = analytics;
    }

    public String getDisqus() {
        return disqus;
    }

    public void setDisqus(String disqus) {
        this.disqus = disqus;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isRoot() {
        return getParent() == null;
    }

    public int getLevel() {
        return isRoot() ? 0 : getParent().getLevel()+1;
    }

    public String getPath() {
        return isRoot() ? "" : getParent().getPath() + name + "/";
    }

    public File getFile(File root) {
        return isRoot() ? root : new File(getParent().getFile(root), name);
    }

    public String getUrl() {
        if (link != null) {
            return link;
        } else if (isRoot()) {
            return "";
        } else {
            String url = parent.getUrl();
            if (!url.endsWith("/")) {
                url += "/";
            }
            return url + Util.urlEncode(name) + "/";
        }
    }

    public String getUrlPath(Folder origin) {
        String esc = Util.urlEncode(name);
        return isRoot() || getParent() == origin
                ? esc : getParent().getUrlPath(origin) + "/" + esc;
    }

    public String getUrlPath() {
        String esc = Util.urlEncode(name);
        return isRoot() || getParent().isRoot()
                ? esc : getParent().getUrlPath() + "/" + esc;
    }

    public String getAnalyticsId() {
        return analytics != null ? analytics
                : parent != null ? parent.getAnalyticsId() : null;
    }

    public String getDisqusId() {
        return disqus != null ? disqus
                : parent != null ? parent.getDisqusId() : null;
    }

    public Set<Folder> getSubfolders() {
        return new TreeSet<>(folders);
    }

    public Picture getPicture(String name) {
        for (Picture pic: pictures) {
            if (pic.getName().equals(name)) {
                return pic;
            }
        }
        return null;
    }

    public Set<Picture> getPictures() {
        return new TreeSet<>(pictures);
    }

    public Track getTrack(String name) {
        for (Track track: tracks) {
            if (track.getName().equals(name)) {
                return track;
            }
        }
        return null;
    }

    public List<Track> getSubtracks(int maxLevel) {
        List<Track> result = new ArrayList<>();
        for (Folder subfolder: folders) {
            subfolder.addTracksRecursive(maxLevel, result);
        }
        return result;
    }

    public Set<Track> getTracks() {
        return new TreeSet<>(tracks);
    }

    public Set<String> getTags() {
        return new TreeSet<>(tags);
    }

    public boolean addTag(String label) {
        return tags.add(label);
    }

    public boolean removeTag(String label) {
        return tags.remove(label);
    }

    public void removeAllTags() {
        tags.clear();
    }

    public LatLngBounds getBounds() {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Picture pic: pictures) {
            LatLng loc = pic.getLocation();
            if (loc != null) {
                builder.include(loc);
            }
        }
        for (Track track: tracks) {
            builder.include(track.getBounds());
        }
        if (builder.isValid()) {
            return builder.build();
        }
        for (Folder folder: folders) {
            LatLngBounds bounds = folder.getBounds();
            if (bounds != null) {
                builder.include(bounds);
            }
        }
        return builder.isValid() ? builder.build() : null;
    }

    private void addTracksRecursive(int maxLevel, List<Track> result) {
        result.addAll(tracks);
        if (maxLevel > 0) {
            for (Folder subfolder: folders) {
                subfolder.addTracksRecursive(maxLevel-1, result);
            }
        }
    }
}
